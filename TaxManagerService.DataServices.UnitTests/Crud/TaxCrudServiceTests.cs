﻿using Moq;
using NUnit.Framework;
using TaxManagerService.Data;

namespace TaxManagerService.DataServices.Crud.UnitTests
{
    /// <summary>
    /// TODO: Write all the test cases for TaxCrudService
    /// </summary>
    [TestFixture, Category("DataServices")]
    public class TaxCrudServiceTests
    {
        private readonly Mock<TaxManagerContext> _taxManagerContext;

        [SetUp]
        public void SetUp()
        {

        }

        [Test]
        public void GetAsync_ShouldReturnEmptyListIfRecordDoesNotExists()
        {

        }
    }
}
