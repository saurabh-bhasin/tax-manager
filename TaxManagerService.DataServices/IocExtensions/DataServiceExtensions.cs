﻿using CsvHelper.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Globalization;
using TaxManagerService.Contracts;
using TaxManagerService.DataServices.Crud;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.DataServices.Storage;
using TaxManagerService.Jobs;

namespace TaxManagerService.DataServices.IocExtensions
{
    public static class DataServiceExtensions
    {
        private const string CsvDelimiter = ",";

        public static IServiceCollection AddDataServiceExtensions(this IServiceCollection services)
        {
            services.TryAddScoped<IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel>, TaxCrudService>();
            services.TryAddScoped<IBulkDataService, BulkDataService>();
            services.TryAddScoped<IFileUploadJobService, FileUploadJobService>();
            services.TryAddScoped<IFileStorageClient, WindowsFileStorageClient>();
            services.TryAddScoped(typeof(IBulkDataResolver<>), typeof(BulkDataResolver<>));

            var configuration = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = CsvDelimiter,
                IgnoreBlankLines = false,
                HasHeaderRecord = false,
                TrimOptions = TrimOptions.Trim,
            };
            configuration.TypeConverterOptionsCache.GetOptions<string>().NullValues.Add(string.Empty);
            services.AddSingleton(configuration);
            return services;
        }
    }
}
