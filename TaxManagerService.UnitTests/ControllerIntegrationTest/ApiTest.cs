﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxManagerService.Contracts;
using TaxManagerService.Controllers;
using TaxManagerService.Data;
using TaxManagerService.Data.Entities;
using TaxManagerService.Data.IocExtensions;
using TaxManagerService.DataServices.Crud;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.UnitTests.Integration
{
    [TestFixture, Category("ControllerIntegrationTests")]
    public class ApiTests
    {
        private Mock<ILogger<MunicipalTaxController>> _logger;
        private IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel> _crudService;
        private MunicipalTaxController _municipalTaxController;
        private TaxManagerContext _taxManagerContext;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TaxManagerContext>();
            _taxManagerContext = new TaxManagerContext(optionsBuilder.UseInMemoryDatabase("ServiceDatabase").Options);
        }

        [SetUp]
        public void SetUp()
        {
            _ = new SeedData(_taxManagerContext);
            _logger = new Mock<ILogger<MunicipalTaxController>>();
            _crudService = new TaxCrudService(_taxManagerContext);
            _municipalTaxController = new MunicipalTaxController(_logger.Object, _crudService);
        }

        [TearDown]
        public void Cleanup()
        {
            _taxManagerContext.MunicipalTaxes.RemoveRange(_taxManagerContext.MunicipalTaxes.ToList());
            _taxManagerContext.TaxFrequencies.RemoveRange(_taxManagerContext.TaxFrequencies.ToList());
            _taxManagerContext.SaveChanges();
        }

        #region Get v1.0/municipaltax/identifier

        [Test]
        public void GetById_ShouldReturnNotFoundIfNoRecordExists()
        {
            Guid identifier = Guid.NewGuid();

            var result = _municipalTaxController.Get(identifier).Result;
            var notFoundResult = result as NotFoundObjectResult;

            // Returns NotFound
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);
            Assert.AreEqual($"Record for {identifier} does not exist", notFoundResult.Value);
        }

        [Test]
        public void GetById_ShouldReturnSuccessIfRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            var expectedJobResultModel = new MunicipalTaxResultModel
            {
                Identifier = identifier,
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow.Date,
                EndDate = DateTime.UtcNow.AddDays(1).Date,
                TaxFrequency = Contracts.TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };
            _taxManagerContext.MunicipalTaxes.Add(new MunicipalTax
            {
                Identifier = identifier,
                MunicipalityName = "Test",
                StartDate = expectedJobResultModel.StartDate,
                EndDate = expectedJobResultModel.EndDate,
                TaxFrequencyId = (int)Contracts.TaxFrequency.Yearly,
                TaxRate = 0.1
            });
            _taxManagerContext.SaveChangesAsync();

            var result = _municipalTaxController.Get(identifier).Result;

            // Returns Success
            var okResult = result as OkObjectResult;
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            AssertResultModelAreEqual(expectedJobResultModel, okResult.Value as MunicipalTaxResultModel);
        }

        #endregion Get v1.0/municipaltax/identifier

        #region Get v1.0/municipaltax/filter/municipalityName

        [Test]
        public void GetByName_ShouldReturnBadRequestIfWrongNameIsPassed()
        {
            string municipalityName = "Municipality1";

            var result = _municipalTaxController.Get(municipalityName).Result;
            var notFoundResult = result as NotFoundObjectResult;

            // Returns BadRequest with message
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);
            Assert.AreEqual($"No data exists for municipality '{municipalityName}'", notFoundResult.Value);
        }

        [Test]
        public void GetByName_ShouldReturnSuccessIfRecordsExists()
        {
            Guid identifier = Guid.NewGuid();
            string municipalityName = "Municipality1";
            var expectedResultCollection = new List<MunicipalTaxResultModel>{ new MunicipalTaxResultModel
            {
                Identifier = identifier,
                MunicipalityName = municipalityName,
                StartDate = DateTime.UtcNow.Date,
                EndDate = DateTime.UtcNow.AddDays(1).Date,
                TaxFrequency = Contracts.TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            }};

            _taxManagerContext.MunicipalTaxes.Add(new MunicipalTax
            {
                Identifier = identifier,
                MunicipalityName = municipalityName,
                StartDate = expectedResultCollection[0].StartDate,
                EndDate = expectedResultCollection[0].EndDate,
                TaxFrequencyId = (int)Contracts.TaxFrequency.Yearly,
                TaxRate = 0.1
            });
            _taxManagerContext.SaveChangesAsync();

            var result = _municipalTaxController.Get(municipalityName).Result;
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            var resultCollection = okResult.Value as List<MunicipalTaxResultModel>;
            expectedResultCollection.ForEach(expected =>
            {
                var actual = resultCollection.Single(y => expected.Identifier == y.Identifier);
                AssertResultModelAreEqual(expected, actual);
            });
        }

        #endregion v1.0/municipaltax/Get/MunicipalityName

        #region v1.0/municipaltax/Post

        [Test]
        public void Post_ShouldReturnBadRequestIfArrayIsEmpty()
        {
            var municipalTaxInputModel = new MunicipalTaxInputModel
            {
                MunicipalityName = "Test",
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = "WrongTaxFrequency",
                TaxRate = 0.1
            };
            var result = _municipalTaxController.Post(municipalTaxInputModel).Result;
            var badResult = result as BadRequestObjectResult;

            // Returns BadRequest
            Assert.IsNotNull(badResult);
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Tax Schedule '{municipalTaxInputModel.TaxFrequency}' is not a valid schedule.", badResult.Value);
        }

        [Test]
        public void Post_ShouldReturnSuccessIfRecordsExists()
        {
            var municipalTaxInputModel = new MunicipalTaxInputModel
            {
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow.AddDays(1),
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = Contracts.TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };
            var result = _municipalTaxController.Post(municipalTaxInputModel).Result;
            var createdAtResult = result as CreatedAtRouteResult;

            // Returns Success
            Assert.IsNotNull(createdAtResult);
            Assert.AreEqual(201, createdAtResult.StatusCode);
            Assert.IsNotNull(createdAtResult.Value is Guid);
        }

        private void AssertResultModelAreEqual(MunicipalTaxResultModel expected, MunicipalTaxResultModel actual)
        {
            Assert.AreEqual(expected.Identifier, actual.Identifier);
            Assert.AreEqual(expected.StartDate, actual.StartDate);
            Assert.AreEqual(expected.EndDate, actual.EndDate);
            Assert.AreEqual(expected.MunicipalityName, actual.MunicipalityName);
            Assert.AreEqual(expected.TaxFrequency, actual.TaxFrequency);
            Assert.AreEqual(expected.TaxRate, actual.TaxRate);
        }

        #endregion v1.0/municipaltax/Post
    }
}
