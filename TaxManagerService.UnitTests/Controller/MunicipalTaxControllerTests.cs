﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using TaxManagerService.Contracts;
using TaxManagerService.Controllers;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.UnitTests.Controller
{
    [TestFixture, Category("ControllerTests")]
    public class MunicipalTaxControllerTests
    {
        private Mock<ILogger<MunicipalTaxController>> _logger;
        private Mock<IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel>> _crudService;
        private MunicipalTaxController _municipalTaxController;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<MunicipalTaxController>>();
            _crudService = new Mock<IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel>>();
            _municipalTaxController = new MunicipalTaxController(_logger.Object, _crudService.Object);
        }

        #region Get v1.0/municipaltax/identifier

        [Test]
        public void GetById_ShouldReturnNotFoundIfNoRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            string message = "Test message";
            _crudService.Setup(x => x.Exists(identifier, out message)).Returns(false);

            var result = _municipalTaxController.Get(identifier).Result;
            var notFoundResult = result as NotFoundObjectResult;

            // Returns NotFound
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);
            Assert.AreEqual(message, notFoundResult.Value);
        }

        [Test]
        public void GetById_ShouldReturnSuccessIfRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            var municipalTaxResultModel = new MunicipalTaxResultModel
            {
                Identifier = identifier,
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };

            string message = string.Empty;
            _crudService.Setup(x => x.Exists(identifier, out message)).Returns(true);
            _crudService.Setup(x => x.GetAsync(identifier)).ReturnsAsync(municipalTaxResultModel);

            var result = _municipalTaxController.Get(identifier).Result;
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual(municipalTaxResultModel, okResult.Value as MunicipalTaxResultModel);
        }

        [Test]
        public void GetById_ShouldReturnErrorIfServiceThrowsException()
        {
            Guid identifier = Guid.NewGuid();
            string message = "Test message";
            _crudService.Setup(x => x.Exists(identifier, out message)).Returns(true);
            _crudService.Setup(x => x.GetAsync(identifier)).Throws(new SystemException());

            Assert.ThrowsAsync<SystemException>(() => _municipalTaxController.Get(identifier));
        }

        #endregion Get v1.0/municipaltax/Identifier

        #region Get v1.0/municipaltax/filter/municipalityName

        [Test]
        public void GetByName_ShouldReturnNotFoundIfNoRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            string municipalityName = "Municipality1";
            string message = "Test message";
            _crudService.Setup(x => x.Exists(municipalityName, out message)).Returns(false);

            var result = _municipalTaxController.Get(municipalityName).Result;
            var notFoundResult = result as NotFoundObjectResult;

            // Returns NotFound
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);
            Assert.AreEqual(message, notFoundResult.Value);
        }

        [Test]
        public void GetByName_ShouldReturnSuccessIfRecordsExists()
        {
            var jobResultCollection = new List<MunicipalTaxResultModel> { new MunicipalTaxResultModel
            {
                Identifier = Guid.NewGuid(),
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            } };
            string message = string.Empty;
            string municipalityName = "Municipality1";
            _crudService.Setup(x => x.Exists(municipalityName, out message)).Returns(true);
            _crudService.Setup(x => x.GetAsync(municipalityName)).ReturnsAsync(jobResultCollection);

            var result = _municipalTaxController.Get(municipalityName).Result;
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            CollectionAssert.AreEqual(jobResultCollection, okResult.Value as List<MunicipalTaxResultModel>);
        }

        [Test]
        public void GetByName_ShouldReturnErrorIfServiceThrowsException()
        {
            string municipalityName = "Municipality1";
            string message = "Test message";
            _crudService.Setup(x => x.Exists(municipalityName, out message)).Returns(true);
            _crudService.Setup(x => x.GetAsync(municipalityName)).Throws(new SystemException());

            Assert.ThrowsAsync<SystemException>(() => _municipalTaxController.Get(municipalityName));
        }

        #endregion Get v1.0/municipaltax/filter/municipalityName

        #region Post v1.0/municipaltax

        [Test]
        public void Post_ShouldReturnBadRequestIfArrayIsEmpty()
        {
            string message = "Test Message";
            var municipalTaxInputModel = new MunicipalTaxInputModel
            {
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };
            _crudService.Setup(x => x.IsValidPost(municipalTaxInputModel, out message)).Returns(false);

            var result = _municipalTaxController.Post(municipalTaxInputModel).Result;
            var badResult = result as BadRequestObjectResult;

            // Returns BadRequest
            Assert.IsNotNull(badResult);
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual(message, badResult.Value);
        }

        [Test]
        public void Post_ShouldReturnSuccessIfRecordsExists()
        {
            string message = "Test Message";
            Guid identifier = Guid.NewGuid();
            var municipalTaxInputModel = new MunicipalTaxInputModel
            {
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };
            _crudService.Setup(x => x.IsValidPost(municipalTaxInputModel, out message)).Returns(true);
            _crudService.Setup(x => x.CreateAsync(municipalTaxInputModel)).ReturnsAsync(identifier);

            var result = _municipalTaxController.Post(municipalTaxInputModel).Result;
            var okResult = result as CreatedAtRouteResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(201, okResult.StatusCode);
            Assert.AreEqual(identifier, okResult.Value);
        }

        [Test]
        public void Post_ShouldReturnErrorIfServiceThrowsException()
        {
            string message = "Test Message";
            Guid identifier = Guid.NewGuid();
            var municipalTaxInputModel = new MunicipalTaxInputModel
            {
                MunicipalityName = "Test",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(1),
                TaxFrequency = TaxFrequency.Yearly.ToString(),
                TaxRate = 0.1
            };
            _crudService.Setup(x => x.IsValidPost(municipalTaxInputModel, out message)).Returns(true);
            _crudService.Setup(x => x.CreateAsync(municipalTaxInputModel)).Throws(new SystemException());

            Assert.ThrowsAsync<SystemException>(() => _municipalTaxController.Post(municipalTaxInputModel));
        }

        #endregion v1.0/municipaltax/Post
    }
}
