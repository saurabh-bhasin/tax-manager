﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxManagerService.Data.Entities;

namespace TaxManagerService.Data.IocExtensions
{
    public class SeedData
    {
        public SeedData(TaxManagerContext taxManagerContext)
        {
            taxManagerContext.TaxFrequencies.AddRange(
                new TaxFrequency { Identifier = 1, Code = "Daily" },
                new TaxFrequency { Identifier = 2, Code = "Weekly" },
                new TaxFrequency { Identifier = 3, Code = "Monthly" },
                new TaxFrequency { Identifier = 4, Code = "Yearly" });
            taxManagerContext.SaveChanges();
        }
    }
}
