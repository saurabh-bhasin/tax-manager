﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using TaxManagerService.Data.Entities;

namespace TaxManagerService.Jobs
{
    public interface IFileUploadJobService
    {
        Task<FileUploadJob> GetRecentJobAsync();
        Task UpdateJobAsync(FileUploadJob job);
        bool IsValidFile(IFormFile formFile, out string message);
        Task<FileUploadJob> UploadCsvDataAsync(string fileName, IFormFile formFile);
    }
}