﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using TaxManagerService.Contracts;

namespace TaxManagerService.Filters.Constraints
{
    public sealed class JobStatusConstraint : IRouteConstraint
    {
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var candidate = values[routeKey]?.ToString();
            return Enum.IsDefined(typeof(JobStatus), candidate?.ToUpper());
        }
    }
}
