﻿using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System.IO;
using System.Linq;
using TaxManagerService.Contracts;
using TaxManagerService.Data;
using TaxManagerService.Data.IocExtensions;
using TaxManagerService.DataServices.Crud;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.DataServices.UnitTests.Crud
{
    /// <summary>
    /// TODO: Add more test cases for BulkDataServiceTests
    /// </summary>
    [TestFixture, Category("DataServices")]
    public class BulkDataServiceTests
    {
        private TaxManagerContext _taxManagerContext;
        private Mock<IFileStorageClient> _fileStorageClient;
        private Mock<IBulkDataResolver<MunicipalTaxInputModel>> _bulkDataResolver;
        private BulkDataService _bulkDataService;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TaxManagerContext>();
            _taxManagerContext = new TaxManagerContext(optionsBuilder.UseInMemoryDatabase("ServiceDatabase").Options);
        }

        [SetUp]
        public void SetUp()
        {
            _ = new SeedData(_taxManagerContext);
            _fileStorageClient = new Mock<IFileStorageClient>();
            _bulkDataResolver = new Mock<IBulkDataResolver<MunicipalTaxInputModel>>();
            _bulkDataService = new BulkDataService(_fileStorageClient.Object, _bulkDataResolver.Object, _taxManagerContext);
        }

        [TearDown]
        public void Cleanup()
        {
            _taxManagerContext.MunicipalTaxes.RemoveRange(_taxManagerContext.MunicipalTaxes.ToList());
            _taxManagerContext.TaxFrequencies.RemoveRange(_taxManagerContext.TaxFrequencies.ToList());
            _taxManagerContext.SaveChanges();
        }

        [Test]
        public void CreateRecordsAsync_ShouldReturnZeroIfStreamIsEmpty()
        {
            string fileName = "TestFileName";
            string folderPrefix = "FolderPrefix";
            //Mock<Stream> stream = new Mock<Stream>();
            _fileStorageClient.Setup(x => x.ReadFile(fileName, folderPrefix)).Returns((Stream)null);

            //Verify no records are updated
            var records = _bulkDataService.CreateRecordsAsync(fileName, folderPrefix).Result;
            Assert.AreEqual(0, records);
        }

        [Test]
        public void CreateRecordsAsync_ShouldReturnZeroIfFileIsEmpty()
        {
            string fileName = "TestFileName";
            string folderPrefix = "FolderPrefix";
            Mock<Stream> stream = new Mock<Stream>();
            _fileStorageClient.Setup(x => x.ReadFile(fileName, folderPrefix)).Returns(stream.Object);
            _bulkDataResolver.Setup(x => x.Read()).Returns(false);

            //Verify no records are updated
            var records = _bulkDataService.CreateRecordsAsync(fileName, folderPrefix).Result;
            Assert.AreEqual(0, records);
        }

        [Test]
        public void CreateRecordsAsync_ThrowExceptionIfNoTaxFrequencyIsFound()
        {
            string fileName = "TestFileName";
            string folderPrefix = "FolderPrefix";
            Mock<Stream> stream = new Mock<Stream>();
            _fileStorageClient.Setup(x => x.ReadFile(fileName, folderPrefix)).Returns(stream.Object);
            _bulkDataResolver.Setup(x => x.Read()).Returns(true);
            _bulkDataResolver.Setup(x => x.GetInput()).Returns(new MunicipalTaxInputModel { TaxFrequency = "WrongFrequency" });

            //Verify throws exception
            Assert.ThrowsAsync<InvalidDataException>(() => _bulkDataService.CreateRecordsAsync(fileName, folderPrefix), "No such tax frequency exists");
        }

        [Test]
        public void CreateRecordsAsync_CreateRecordIfDataIsValid()
        {
            string fileName = "TestFileName";
            string folderPrefix = "FolderPrefix";
            Mock<Stream> stream = new Mock<Stream>();
            _fileStorageClient.Setup(x => x.ReadFile(fileName, folderPrefix)).Returns(stream.Object);
            _bulkDataResolver.SetupSequence(x => x.Read()).Returns(true).Returns(false);
            _bulkDataResolver.Setup(x => x.GetInput()).Returns(new MunicipalTaxInputModel { TaxFrequency = "Yearly" });

            //Verify record
            var records = _bulkDataService.CreateRecordsAsync(fileName, folderPrefix).Result;
            Assert.AreEqual(1, records);
        }

        [Test]
        public void CreateRecordsAsync_CreateTwoRecordsIfDataIsValid()
        {
            string fileName = "TestFileName";
            string folderPrefix = "FolderPrefix";
            Mock<Stream> stream = new Mock<Stream>();
            _fileStorageClient.Setup(x => x.ReadFile(fileName, folderPrefix)).Returns(stream.Object);
            _bulkDataResolver.SetupSequence(x => x.Read()).Returns(true).Returns(true).Returns(false);
            _bulkDataResolver.Setup(x => x.GetInput()).Returns(new MunicipalTaxInputModel { TaxFrequency = "Yearly" });

            //Verify record
            var records = _bulkDataService.CreateRecordsAsync(fileName, folderPrefix).Result;
            Assert.AreEqual(2, records);
        }
    }
}
