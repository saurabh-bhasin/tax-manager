﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace TaxManagerService.Filters.Exception
{
    public class UnknownExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var objResult = new ObjectResult("Unknown error occured")
            {
                StatusCode = (int)HttpStatusCode.InternalServerError,
            };
            context.Result = objResult;
            context.ExceptionHandled = true;
        }
    }
}