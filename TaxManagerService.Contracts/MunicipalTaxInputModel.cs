﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaxManagerService.Contracts
{
    public class MunicipalTaxInputModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "MunicipalityName Field is required")]
        public string MunicipalityName { get; set; }
        [Required(AllowEmptyStrings =false, ErrorMessage = "TaxFrequency Field is required")]
        public string TaxFrequency { get; set; }
        [Required(ErrorMessage = "StartDate Field is required")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "EndDate Field is required")]
        public DateTime EndDate { get; set; }
        [Required]
        public double TaxRate { get; set; }
    }
}
