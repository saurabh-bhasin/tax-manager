﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaxManagerService.Data;
using TaxManagerService.Data.Entities;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.Jobs;

namespace TaxManagerService.DataServices.Crud
{
    public class FileUploadJobService : IFileUploadJobService
    {
        private readonly IFileStorageClient _fileStorageClient;
        private readonly TaxManagerContext _taxManagerContext;

        public FileUploadJobService(TaxManagerContext taxManagerContext, IFileStorageClient fileStorageClient)
        {
            _taxManagerContext = taxManagerContext;
            _fileStorageClient = fileStorageClient;
        }

        public async Task<FileUploadJob> UploadCsvDataAsync(string fileName, IFormFile formFile)
        {
            var fileUploadJob = new FileUploadJob
            {
                Identifier = Guid.NewGuid(),
                FileName = fileName,
                FilePathPrefix = "MunicipalTaxData",
                Status = FileUploadStatus.Created.ToString()
            };

            _taxManagerContext.FileUploadJobs.Add(fileUploadJob);

            var uploadTask = await _fileStorageClient.UploadFileAsync(fileName, fileUploadJob.FilePathPrefix, formFile);
            if (!uploadTask)
            {
                return new FileUploadJob { Error = "File already exists." };
            }
            var saveTask = await _taxManagerContext.SaveChangesAsync();

            return _taxManagerContext.FileUploadJobs.FirstOrDefault(x => x.Identifier == fileUploadJob.Identifier);
        }

        public bool IsValidFile(IFormFile formFile, out string message)
        {
            message = string.Empty;
            if (formFile is null)
            {
                message = "Missing data file.";
            }
            else if (!formFile.FileName.Contains(".csv"))
            {
                message = "Not valid file";
                return false;
            }

            return true;
        }

        public async Task<FileUploadJob> GetRecentJobAsync()
        {
            return await _taxManagerContext.FileUploadJobs
                .Where(x => x.Status == FileUploadStatus.Created.ToString())
                .FirstOrDefaultAsync();
        }

        public async Task UpdateJobAsync(FileUploadJob job)
        {
            var existing = await _taxManagerContext.FileUploadJobs.Where(x => x.Identifier == job.Identifier).FirstOrDefaultAsync();
            existing = job;
        }
    }
}
