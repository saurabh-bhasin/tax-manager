﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaxManagerService.Contracts
{
    public class TaxQueryRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "MunicipalityName Field is required")]
        public string MunicipalityName { get; set; }
        [Required(ErrorMessage = "TaxDate Field is required")]
        public DateTime TaxDate { get; set; }
    }
}
