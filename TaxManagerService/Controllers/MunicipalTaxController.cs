﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using TaxManagerService.Contracts;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.Filters;
using TaxManagerService.Filters.Exception;
using TaxManagerService.Jobs;

namespace TaxManagerService.Controllers
{
    [ApiController]
    [Route("api/v1.0/municipaltax")]
    [UnknownExceptionFilter]
    [BadRequestResultExceptionFilter]
    [Produces("application/json")]
    public class MunicipalTaxController : ControllerBase
    {
        private readonly IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel> _jobDataService;
        private readonly ILogger<MunicipalTaxController> _logger;

        public MunicipalTaxController(ILogger<MunicipalTaxController> logger,
            IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel> jobDataService)
        {
            _logger = logger;
            _jobDataService = jobDataService;
        }

        [HttpGet("{identifier:guid}", Name = "GetMuniciaplTaxById")]
        public async Task<IActionResult> Get(Guid identifier)
        {
            if (!_jobDataService.Exists(identifier, out string message))
            {
                _logger.LogError(message);
                return new NotFoundObjectResult(message);
            }
            return Ok(await _jobDataService.GetAsync(identifier));
        }

        [HttpGet("filter/{municipalityName}")]
        public async Task<IActionResult> Get(string municipalityName)
        {
            if (!_jobDataService.Exists(municipalityName, out string message))
            {
                return new NotFoundObjectResult(message);
            }
            return Ok(await _jobDataService.GetAsync(municipalityName));
        }

        [HttpPost()]
        public async Task<IActionResult> Post([FromBody] MunicipalTaxInputModel municipalTaxInputModel)
        {
            if (!_jobDataService.IsValidPost(municipalTaxInputModel, out string message))
            {
                _logger.LogError(message);
                return new BadRequestObjectResult(message);
            }
            var identifier = await _jobDataService.CreateAsync(municipalTaxInputModel);
            return CreatedAtRoute("GetMuniciaplTaxById", new { Controller = "municipaltax", identifier }, identifier);
        }

        [HttpPut("{identifier:guid}")]
        public async Task<IActionResult> Put(Guid identifier, [FromBody] MunicipalTaxInputModel municipalTaxInputModel)
        {
            if (!_jobDataService.Exists(identifier, out string message))
            {
                _logger.LogError(message);
                return new BadRequestObjectResult(message);
            }
            identifier = await _jobDataService.UpdateAsync(identifier, municipalTaxInputModel);
            return AcceptedAtRoute("GetMuniciaplTaxById", new { Controller = "municipaltax", identifier }, identifier);
        }

        [HttpPost("csv")]
        [FileUploadOperation.FileContentType]
        public async Task<IActionResult> Post([FromServices] IFileUploadJobService uploadDataService, [FromForm(Name = "file")] IFormFile file)
        {
            if (!uploadDataService.IsValidFile(file, out string message))
            {
                _logger.LogError(message);
                return new BadRequestObjectResult(message);
            }

            var fileUploadJob = await uploadDataService.UploadCsvDataAsync(file.FileName, file);
            if (fileUploadJob != null && !string.IsNullOrEmpty(fileUploadJob.Error))
            {
                return new BadRequestObjectResult(fileUploadJob.Error);
            }

            return Ok(fileUploadJob);
        }
    }
}
