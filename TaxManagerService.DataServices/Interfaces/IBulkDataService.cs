﻿using System.Threading.Tasks;

namespace TaxManagerService.DataServices.Interfaces
{
    public interface IBulkDataService
    {
        Task<int> CreateRecordsAsync(string fileName, string filePathPrefix);
    }
}
