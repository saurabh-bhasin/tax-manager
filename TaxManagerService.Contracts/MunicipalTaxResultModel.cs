﻿using System;

namespace TaxManagerService.Contracts
{
    public class MunicipalTaxResultModel
    {
        public Guid Identifier { get; set; }
        public string MunicipalityName { get; set; }
        public string TaxFrequency { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double TaxRate { get; set; }
    }
}