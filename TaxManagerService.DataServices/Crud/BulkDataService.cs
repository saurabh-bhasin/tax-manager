﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using TaxManagerService.Contracts;
using TaxManagerService.Data;
using TaxManagerService.Data.Entities;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.DataServices.Crud
{
    public class BulkDataService : IBulkDataService
    {
        private readonly IFileStorageClient _fileStorageClient;
        private readonly IBulkDataResolver<MunicipalTaxInputModel> _bulkDataResolver;
        private readonly TaxManagerContext _taxManagerContext;

        public BulkDataService(IFileStorageClient fileStorageClient, IBulkDataResolver<MunicipalTaxInputModel> bulkDataResolver,
            TaxManagerContext taxManagerContext)
        {
            _fileStorageClient = fileStorageClient;
            _bulkDataResolver = bulkDataResolver;
            _taxManagerContext = taxManagerContext;
        }

        public async Task<int> CreateRecordsAsync(string fileName, string filePathPrefix)
        {
            using var stream = _fileStorageClient.ReadFile(fileName, filePathPrefix);
            if (stream != null)
            {
                _bulkDataResolver.Start(stream);
                while (_bulkDataResolver.Read())
                {
                    var input = _bulkDataResolver.GetInput();
                    if (IsValidInput(input, out _))
                    {
                        AddRecord(input);
                    }
                }

                return await _taxManagerContext.SaveChangesAsync();
            }

            return default;
        }

        private void AddRecord(MunicipalTaxInputModel input)
        {
            var taxFrequency = _taxManagerContext.TaxFrequencies.FirstOrDefault(x => x.Code == input.TaxFrequency);
            if (taxFrequency == null)
            {
                throw new InvalidDataException("No such tax frequency exists");
            }

            var taxData = new MunicipalTax
            {
                MunicipalityName = input.MunicipalityName,
                StartDate = input.StartDate.Date,
                EndDate = input.EndDate.Date,
                TaxFrequencyId = taxFrequency.Identifier,
                TaxRate = input.TaxRate
            };

            _taxManagerContext.MunicipalTaxes.Add(taxData);
        }

        /// <summary>
        /// TODO: Apply validations as per requirements
        /// </summary>
        /// <param name="input"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        private bool IsValidInput(MunicipalTaxInputModel input, out List<string> errors)
        {
            errors = new List<string>();
            if (input == null)
            {
                throw new InvalidDataException("Invalid data");
            }

            return true;
        }
    }
}
