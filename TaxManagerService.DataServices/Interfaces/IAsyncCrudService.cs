﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaxManagerService.Contracts;

namespace TaxManagerService.DataServices.Interfaces
{
    public interface IAsyncCrudService<TInputModel, TResultModel> where TInputModel : class where TResultModel : class
    {
        bool Exists(Guid identifier, out string message);
        Task<TResultModel> GetAsync(Guid identifier);
        bool ValidateFilter(TaxQueryRequest inputModel, out string message);
        Task<IEnumerable<TResultModel>> GetAsync(string municipalityName);
        bool Exists(string municipalityName, out string message);
        bool IsValidPost(TInputModel inputModel, out string message);
        Task<Guid> CreateAsync(TInputModel inputModel);
        Task<Guid> UpdateAsync(Guid identifier, MunicipalTaxInputModel inputModel);
        Task<double?> GetTaxRateAsync(TaxQueryRequest inputModel);
    }
}