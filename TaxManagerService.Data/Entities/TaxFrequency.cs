﻿using System.Collections.Generic;

namespace TaxManagerService.Data.Entities
{

    public class TaxFrequency
    {
        public int Identifier { get; set; }
        public string Code { get; set; }

        public virtual ICollection<MunicipalTax> MunicipalTaxes { get; set; }
    }
}
