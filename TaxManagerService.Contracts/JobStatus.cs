﻿namespace TaxManagerService.Contracts
{
    public enum JobStatus
    {
        PENDING,
        COMPLETED,
        FAILED
    }
}
