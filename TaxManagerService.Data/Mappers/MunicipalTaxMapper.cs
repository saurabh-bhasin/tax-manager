﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaxManagerService.Data.Entities;

namespace TaxManagerService.Data.Mappers
{
    internal class MunicipalTaxMapper : IEntityTypeConfiguration<MunicipalTax>
    {
        public void Configure(EntityTypeBuilder<MunicipalTax> builder)
        {
            builder.ToTable("MunicipalTax");
            builder.HasKey(x => x.Identifier);
            builder.Property(x => x.Identifier).IsRequired().HasColumnName("Identifier");
            builder.Property(x => x.TaxFrequencyId).IsRequired().HasColumnName("TaxFrequencyId");
            builder.Property(x => x.MunicipalityName).IsRequired().HasColumnName("MunicipalityName");
            builder.Property(x => x.StartDate).IsRequired().HasColumnName("StartDate");
            builder.Property(x => x.EndDate).IsRequired().HasColumnName("EndDate");
            builder.Property(x => x.TaxRate).IsRequired().HasColumnName("TaxRate");

            builder.HasOne(x => x.TaxFrequency).WithMany(x => x.MunicipalTaxes).HasForeignKey(x => x.TaxFrequencyId)
                .HasConstraintName("Fk_MunicipalTax_Frequency").OnDelete(DeleteBehavior.NoAction);
        }
    }
}
