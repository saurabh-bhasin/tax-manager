﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaxManagerService.Data.Entities;

namespace TaxManagerService.Data.Mappers
{
    internal class FileUploadJobMapper : IEntityTypeConfiguration<FileUploadJob>
    {
        public void Configure(EntityTypeBuilder<FileUploadJob> builder)
        {
            builder.ToTable("FileUploadJob");
            builder.HasKey(x => x.Identifier);
            builder.Property(x => x.Identifier).IsRequired().HasColumnName("Identifier");
            builder.Property(x => x.FileName).IsRequired().HasColumnName("FileName");
            builder.Property(x => x.FilePathPrefix).IsRequired().HasColumnName("FilePathPrefix");
            builder.Property(x => x.FileType).IsRequired().HasColumnName("FileType");
            builder.Property(x => x.Status).IsRequired().HasColumnName("Status");
            builder.Property(x => x.Error).IsRequired().HasColumnName("Error");
            builder.Property(x => x.CreationDateTime).IsRequired().HasColumnName("CreationDateTime");
        }
    }
}
