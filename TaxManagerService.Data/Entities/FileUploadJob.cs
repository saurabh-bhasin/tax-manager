﻿using System;

namespace TaxManagerService.Data.Entities
{
    public class FileUploadJob
    {
        public Guid Identifier { get; set; }
        public string FileName { get; set; }
        public string FilePathPrefix { get; set; }
        public string FileType { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
        public DateTime CreationDateTime { get; set; }
    }
}
