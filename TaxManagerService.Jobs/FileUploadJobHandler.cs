﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using TaxManagerService.Contracts;
using TaxManagerService.Data.Entities;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.Jobs.Interfaces;

namespace TaxManagerService.Jobs
{
    public class FileUploadJobHandler : IJobHandler
    {
        private readonly ILogger _logger;
        private readonly IBulkDataService _bulkDataService;
        private readonly IFileUploadJobService _jobDataService;

        public FileUploadJobHandler(ILogger<FileUploadJobHandler> logger, IBulkDataService bulkDataService,
            IFileUploadJobService jobDataService)
        {
            _logger = logger;
            _bulkDataService = bulkDataService;
            _jobDataService = jobDataService;
        }

        public async Task Handle(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var job = await _jobDataService.GetRecentJobAsync();
                if (job != null)
                {
                    _logger.LogInformation($"Found 1 {JobStatus.PENDING} job for processing.");
                    try
                    {
                        job.Status = FileUploadStatus.InProgress.ToString();
                        var recordCount = await _bulkDataService.CreateRecordsAsync(job.FileName, job.FilePathPrefix);
                        if (recordCount > 0)
                        {
                            job.Status = FileUploadStatus.Success.ToString();
                        }
                        else
                        {
                            job.Error = "Created record count is 0.";
                            job.Status = FileUploadStatus.Failed.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Job Failed while processing {JobStatus.PENDING} jobs.", ex);
                        job.Status = FileUploadStatus.Failed.ToString();
                        job.Error = ex.Message;
                    }
                    await _jobDataService.UpdateJobAsync(job);
                }
                else
                {
                    // Log the info and exit
                    _logger.LogInformation($"No {JobStatus.PENDING} job found.");
                }
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}