﻿namespace TaxManagerService.Data.Entities
{
    public enum FileUploadStatus
    {
        Created,
        InProgress,
        Success,
        Failed
    }
}
