﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.DataServices.Storage
{
    public class WindowsFileStorageClient : IFileStorageClient
    {
        public Stream ReadFile(string fileName, string filePathPrefix)
        {
            if (!File.Exists($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}/{fileName}"))
            {
                return null;
            }

            return new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}/{fileName}", FileMode.Open);
        }

        public async Task<bool> UploadFileAsync(string fileName, string filePathPrefix, IFormFile file)
        {
            if (!Directory.Exists($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}"))
            {
                Directory.CreateDirectory($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}");
            }

            if (!File.Exists($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}/{fileName}"))
            {
                using var fileStream = new FileStream($"{AppDomain.CurrentDomain.BaseDirectory}/{filePathPrefix}/{fileName}", FileMode.Create, FileAccess.Write);
                await file.OpenReadStream().CopyToAsync(fileStream);
                fileStream.Flush();
                return true;
            }

            return false;
        }
    }
}
