using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using TaxManagerService.Data.IocExtensions;
using TaxManagerService.DataServices.IocExtensions;
using TaxManagerService.Filters;
using TaxManagerService.Filters.Constraints;
using TaxManagerService.Filters.Exception;
using TaxManagerService.Jobs.IocExtensions;

namespace TaxManagerService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("api", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Tax Manager Service API",
                    Description = "Tax Manager Service API"
                });
                c.OperationFilter<FileUploadOperation>();
            });
            services.AddMvc(options => options.Filters.Add(new BadRequestResultExceptionFilterAttribute()));
            services.Configure<RouteOptions>(x => x.ConstraintMap.Add("jobstatusconstraint", typeof(JobStatusConstraint)));
            services.AddBackgroundJobExtensions()
                .AddDataServiceExtensions()
                .AddDatabaseExtensions(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            SetupPathBase(app, Configuration["PathBase"]);
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void SetupPathBase(IApplicationBuilder app, string pathBase)
        {
            var trimmedPathBase = pathBase.TrimStart('/').TrimEnd('/');
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                {
                    c.SerializeAsV2 = true;
                    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{pathBase}" } };
                });
                c.RouteTemplate = $"{trimmedPathBase}/documentation/{{documentName}}/swagger";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/{trimmedPathBase}/documentation/api/swagger", "Tax Manager API");
                c.RoutePrefix = $"{trimmedPathBase}/swagger";
            });
            app.UsePathBase(pathBase);
        }
    }
}
