﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TaxManagerService.Contracts;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.Filters.Exception;

namespace TaxManagerService.Controllers
{
    [Route("api/v1.0/taxquery")]
    [ApiController]
    [UnknownExceptionFilter]
    [BadRequestResultExceptionFilter]
    public class TaxQueryController : ControllerBase
    {
        private readonly IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel> _jobDataService;
        private readonly ILogger<TaxQueryController> _logger;

        public TaxQueryController(ILogger<TaxQueryController> logger,
            IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel> jobDataService)
        {
            _logger = logger;
            _jobDataService = jobDataService;
        }

        [HttpPost()]
        public async Task<IActionResult> Post(TaxQueryRequest taxQueryRequest)
        {
            if (!_jobDataService.ValidateFilter(taxQueryRequest, out string message))
            {
                _logger.LogError(message);
                return new BadRequestObjectResult(message);
            }
            return Ok(await _jobDataService.GetTaxRateAsync(taxQueryRequest));
        }
    }
}
