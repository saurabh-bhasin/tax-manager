﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaxManagerService.Data.Entities;

namespace TaxManagerService.Data.Mappers
{
    internal class TaxFrequencyMapper : IEntityTypeConfiguration<TaxFrequency>
    {
        public void Configure(EntityTypeBuilder<TaxFrequency> builder)
        {
            builder.ToTable("TaxFrequency");
            builder.HasKey(x => x.Identifier);
            builder.Property(x => x.Identifier).IsRequired().HasColumnName("Identifier");
            builder.Property(x => x.Code).IsRequired().HasColumnName("Code");
        }
    }
}