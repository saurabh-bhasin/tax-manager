﻿using Microsoft.EntityFrameworkCore;
using TaxManagerService.Data.Entities;
using TaxManagerService.Data.Mappers;

namespace TaxManagerService.Data
{
    public class TaxManagerContext : DbContext
    {
        public TaxManagerContext(DbContextOptions<TaxManagerContext> options)
        : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.EnableSensitiveDataLogging(true);
            }
        }

        public virtual DbSet<MunicipalTax> MunicipalTaxes { get; set; }
        public virtual DbSet<FileUploadJob> FileUploadJobs { get; set; }
        public virtual DbSet<TaxFrequency> TaxFrequencies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MunicipalTaxMapper());
            modelBuilder.ApplyConfiguration(new FileUploadJobMapper());
            modelBuilder.ApplyConfiguration(new TaxFrequencyMapper());
        }
    }
}
