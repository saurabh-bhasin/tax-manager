using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Threading;
using TaxManagerService.Contracts;
using TaxManagerService.Data.Entities;
using TaxManagerService.DataServices.Interfaces;
using TaxManagerService.Jobs;

namespace TaxManagerService.UnitTests.Jobs
{
    [TestFixture, Category("BackgroundJobTests")]
    public class FileUploadJobHandlerTests
    {
        private Mock<ILogger<FileUploadJobHandler>> _logger;
        private Mock<IFileUploadJobService> _fileUploadJobService;
        private FileUploadJobHandler _fileUploadJobHandler;
        private Mock<IBulkDataService> _bulkDataService;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<FileUploadJobHandler>>();
            _fileUploadJobService = new Mock<IFileUploadJobService>();
            _bulkDataService = new Mock<IBulkDataService>();
            _fileUploadJobHandler = new FileUploadJobHandler(_logger.Object, _bulkDataService.Object, _fileUploadJobService.Object);
        }

        [Test]
        public void ShouldNotCallUpdateJobServiceIfNoPendingJobExists()
        {
            var task = _fileUploadJobHandler.Handle(new CancellationToken(false));

            // Should not call GetRecentJobAsync service
            _bulkDataService.Verify(x => x.CreateRecordsAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _fileUploadJobService.Verify(x => x.UpdateJobAsync(It.IsAny<FileUploadJob>()), Times.Never);
        }

        [Test]
        public void VerifyJobOutputIfPendingJobExists()
        {
            Guid identifier = Guid.NewGuid();
            var job = new FileUploadJob
            {
                Identifier = identifier,
                FileName = "TestFile1",
                FilePathPrefix = "TestFilePrefix",
                FileType = FileType.Csv.ToString(),
                Status = "Created",
                CreationDateTime = DateTime.UtcNow
            };

            _fileUploadJobService.Setup(x => x.GetRecentJobAsync()).ReturnsAsync(job);
            _bulkDataService.Setup(x => x.CreateRecordsAsync(job.FileName, job.FilePathPrefix)).ReturnsAsync(1);

            var task = _fileUploadJobHandler.Handle(new CancellationToken(false));

            // Should call GetRecentJobAsync and UpdateJobAsync once
            _bulkDataService.Verify(x => x.CreateRecordsAsync(job.FileName, job.FilePathPrefix), Times.Once);
            _fileUploadJobService.Verify(x => x.UpdateJobAsync(job), Times.Once);
        }
    }
}
