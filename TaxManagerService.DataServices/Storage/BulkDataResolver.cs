﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.IO;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.DataServices.Storage
{
    public class BulkDataResolver<TRecord> : IBulkDataResolver<TRecord>, IDisposable
    {
        private readonly CsvConfiguration _configuration;
        private Stream _stream;
        private StreamReader _streamReader;
        private CsvReader _csvReader;

        public BulkDataResolver(CsvConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Start(Stream stream)
        {
            if (_stream != null || _streamReader != null || _csvReader != null)
            {
                throw new InvalidOperationException($"Finish Reading '{nameof(stream)}' first");
            }
            _stream = stream;
            _streamReader = new StreamReader(_stream);
            _csvReader = new CsvReader(_streamReader, _configuration);
        }

        public bool Read()
        {
            var returnValue = _csvReader?.Read() ?? false;
            if (!returnValue)
            {
                Dispose();
            }
            return returnValue;
        }

        public TRecord GetInput()
        {
            return _csvReader.GetRecord<TRecord>();
        }

        public void Dispose()
        {
            _stream?.Dispose();
            _streamReader?.Dispose();
            _csvReader?.Dispose();
            _stream = null;
            _streamReader = null;
            _csvReader = null;
        }
    }
}
