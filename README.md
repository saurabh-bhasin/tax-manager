# tax-manager-service

This service helps the users to perform crud operation on municipality tax data and upload tax schedule files in csv format.

#This service contains three main components:

#Produce Api 

#Six main endpoints for Producer api as per the functional requirements are:
1. GET: http://localhost:5002/api/v1.0/municipaltax/{identifier}
2. GET: http://localhost:5002//api/v1.0/municipaltax/filter/{municipalityName}
3. POST: http://localhost:5002/api/v1.0/municipaltax/tax-query
4. POST: http://localhost:5002/api/v1.0/municipaltax
5. POST: http://localhost:5002/api/v1.0/municipaltax/csv
6. PUT: http://localhost:5002/api/v1.0/municipaltax/{identifier}

#Background Job (Timed Job running every one second) : Aim is to process the uploaded CSV files. Root directory contains one sample file ie. Data.csv

#Swagger UI: Acts as a consumer of the APIs. It helps testing of apis manually.

#Quick steps to Build & Run:
1. Clone the application in local using git clone.
2. Open the solution(TaxManagerService.sln) in Visual Studio 2019.
3. Build the solution using Build option in Solution explorer.
4. Choose TaxManagerService as the self hosted service in the execution option in Visual Studio.
5. Step 4 will start a command window and Swagger UI in the default web explorer.
6. Now all the apis will be excessible via Swagger UI which will act as complete documentation for APIs and the background job will be running in background.

#Architecture: 
1. The application architecture uses in memory sql server data wrapped under TaxManagerContext to query via APIs and processing background Job. 
2. Application is based on DI pattern to enable effective testing and maintenance in future changes.
3. Csv files uploaded are processed in background and user is not waiting for the job to complete.
4. Added some unit tests cases to demonstrate the writing unit test case. Do not consider it as final. Apart from writing tests for Controller and SortingJobHandler, I have covered integration tests for Controller to see overall testing.

#Improvements in future (lacking currently due to limited time):
1. Implementing Repository Pattern
2. In-depth coverage of test cases
3. Replace in memory database with sql server with migrations.
4. Writing api integration tests by adding docker support in the application.
5. Adding Job Apis