﻿using Microsoft.Extensions.DependencyInjection;
using TaxManagerService.Jobs.Interfaces;

namespace TaxManagerService.Jobs.IocExtensions
{
    public static class BackgroundJobExtensions
    {
        public static IServiceCollection AddBackgroundJobExtensions(this IServiceCollection services)
        {
            services.AddScoped<IJobHandler, FileUploadJobHandler>();
            return services;
        }
    }
}
