﻿using System;
using System.Collections.Generic;

namespace TaxManagerService.Data.Entities
{
    public class MunicipalTax
    {
        public Guid Identifier { get; set; }
        public string MunicipalityName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double TaxRate { get; set; }

        public int TaxFrequencyId { get; set; }
        public virtual TaxFrequency TaxFrequency { get; set; }
    }

}