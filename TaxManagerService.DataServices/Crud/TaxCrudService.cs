﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxManagerService.Contracts;
using TaxManagerService.Data;
using TaxManagerService.Data.Entities;
using TaxManagerService.DataServices.Interfaces;

namespace TaxManagerService.DataServices.Crud
{
    public class TaxCrudService : IAsyncCrudService<MunicipalTaxInputModel, MunicipalTaxResultModel>
    {
        private readonly TaxManagerContext _taxManagerContext;

        public TaxCrudService(TaxManagerContext taxManagerContext)
        {
            _taxManagerContext = taxManagerContext ?? throw new ArgumentNullException(nameof(TaxManagerContext));
        }

        public bool Exists(Guid identifier, out string message)
        {
            message = string.Empty;
            if (!_taxManagerContext.MunicipalTaxes.Any(x => x.Identifier == identifier))
            {
                message = $"Record for {identifier} does not exist";
                return false;
            }

            return true;
        }

        public async Task<MunicipalTaxResultModel> GetAsync(Guid identifier)
            => await _taxManagerContext.MunicipalTaxes.Include(x => x.TaxFrequency)
            .Where(x => x.Identifier == identifier).Select(x => new MunicipalTaxResultModel
            {
                Identifier = x.Identifier,
                MunicipalityName = x.MunicipalityName,
                TaxFrequency = x.TaxFrequency.Code,
                StartDate = x.StartDate.Date,
                EndDate = x.EndDate.Date,
                TaxRate = x.TaxRate
            }).FirstOrDefaultAsync();

        public bool ValidateFilter(TaxQueryRequest inputModel, out string message)
        {
            message = string.Empty;
            if (!_taxManagerContext.MunicipalTaxes.Any(x => x.MunicipalityName.Equals(inputModel.MunicipalityName, StringComparison.OrdinalIgnoreCase) &&
                inputModel.TaxDate >= x.StartDate && inputModel.TaxDate <= x.EndDate))
            {
                message = $"Data does not exist for {inputModel.MunicipalityName} and date {inputModel.TaxDate}";
                return false;
            }

            return true;
        }

        public async Task<double?> GetTaxRateAsync(TaxQueryRequest inputModel)
        {
            var municipalTax = await _taxManagerContext.MunicipalTaxes
                .Where(x => x.MunicipalityName.Equals(inputModel.MunicipalityName, StringComparison.OrdinalIgnoreCase) &&
                inputModel.TaxDate >= x.StartDate && inputModel.TaxDate <= x.EndDate).OrderBy(x => x.TaxFrequencyId).FirstOrDefaultAsync();
            if (municipalTax != null)
            {
                return municipalTax.TaxRate;
            }

            return null;
        }

        public async Task<IEnumerable<MunicipalTaxResultModel>> GetAsync(string municipalityName)
            => await _taxManagerContext.MunicipalTaxes.Include(x => x.TaxFrequency)
            .Where(x => x.MunicipalityName == municipalityName).Select(x => new MunicipalTaxResultModel
            {
                Identifier = x.Identifier,
                MunicipalityName = x.MunicipalityName,
                TaxFrequency = x.TaxFrequency.Code,
                StartDate = x.StartDate.Date,
                EndDate = x.EndDate.Date,
                TaxRate = x.TaxRate
            }).ToListAsync();

        public bool Exists(string municipalityName, out string message)
        {
            message = string.Empty;
            if (!_taxManagerContext.MunicipalTaxes.Any(x => x.MunicipalityName == municipalityName))
            {
                message = $"No data exists for municipality '{municipalityName}'";
                return false;
            }

            return true;
        }

        public bool IsValidPost(MunicipalTaxInputModel inputModel, out string message)
        {
            message = string.Empty;
            var taxFrequency = _taxManagerContext.TaxFrequencies.FirstOrDefault(x => x.Code == inputModel.TaxFrequency);
            if (taxFrequency == null)
            {
                message = InvalidTaxFrequency(inputModel.TaxFrequency);
                return false;
            }

            return true;
        }

        private static string InvalidTaxFrequency(string taxFrequency)
            => $"Tax Schedule '{taxFrequency}' is not a valid schedule.";

        public async Task<Guid> CreateAsync(MunicipalTaxInputModel inputModel)
        {
            var taxFrequency = await _taxManagerContext.TaxFrequencies.FirstOrDefaultAsync(x => x.Code == inputModel.TaxFrequency);
            if (taxFrequency == null)
            {
                return Guid.Empty;
            }

            var taxData = new MunicipalTax
            {
                Identifier = Guid.NewGuid(),
                MunicipalityName = inputModel.MunicipalityName,
                StartDate = inputModel.StartDate.Date,
                EndDate = inputModel.EndDate.Date,
                TaxFrequencyId = taxFrequency.Identifier,
                TaxRate = inputModel.TaxRate,
            };

            _taxManagerContext.MunicipalTaxes.Add(taxData);
            await _taxManagerContext.SaveChangesAsync();

            return taxData.Identifier;
        }

        public async Task<Guid> UpdateAsync(Guid identifier, MunicipalTaxInputModel inputModel)
        {
            var taxFrequency = await _taxManagerContext.TaxFrequencies.FirstOrDefaultAsync(x => x.Code == inputModel.TaxFrequency);
            if (taxFrequency == null)
            {
                throw new ArgumentException(InvalidTaxFrequency(inputModel.TaxFrequency));
            }

            var taxData = _taxManagerContext.MunicipalTaxes.FirstOrDefault(x => x.Identifier == identifier);
            if (taxData == null)
            {
                throw new ArgumentException($"Invalid Identifier {identifier}");
            }

            var entry = _taxManagerContext.Entry(taxData);
            entry.State = EntityState.Modified;
            taxData.MunicipalityName = inputModel.MunicipalityName;
            taxData.StartDate = inputModel.StartDate.Date;
            taxData.EndDate = inputModel.EndDate.Date;
            taxData.TaxFrequencyId = taxFrequency.Identifier;
            taxData.TaxRate = inputModel.TaxRate;

            await _taxManagerContext.SaveChangesAsync();

            return taxData.Identifier;
        }
    }
}