﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace TaxManagerService.DataServices.Interfaces
{
    public interface IFileStorageClient
    {
        Task<bool> UploadFileAsync(string fileName, string filePathPrefix, IFormFile file);
        Stream ReadFile(string fileName, string filePathPrefix);
    }
}
