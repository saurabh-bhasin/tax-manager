﻿namespace TaxManagerService.Contracts
{
    public enum TaxFrequency
    {
        Daily = 1,
        Weekly,
        Monthly,
        Yearly
    }
}
