﻿using System.IO;

namespace TaxManagerService.DataServices.Interfaces
{
    public interface IBulkDataResolver<T>
    {
        void Start(Stream stream);
        bool Read();
        T GetInput();
    }
}
