﻿using System.Threading;
using System.Threading.Tasks;

namespace TaxManagerService.Jobs.Interfaces
{
    public interface IJobHandler
    {
        Task Handle(CancellationToken stoppingToken);
    }
}