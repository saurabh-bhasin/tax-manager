﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TaxManagerService.Data.IocExtensions
{
    public static class DatabaseExtensions
    {
        private const string ServiceDatabase = "ServiceDatabase";

        public static IServiceCollection AddDatabaseExtensions(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(ServiceDatabase);
            serviceCollection.AddDbContext<TaxManagerContext>(options => {
                    options.UseInMemoryDatabase(connectionString);
                    options.EnableSensitiveDataLogging(true);
                });
            serviceCollection.AddSingleton(new SeedData(serviceCollection.BuildServiceProvider().GetService<TaxManagerContext>()));
            return serviceCollection;
        }
    }
}
